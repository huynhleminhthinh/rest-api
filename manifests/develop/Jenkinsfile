pipeline {
  /**
   ** @description: Define Extra Kubernetes Pods to be used along with Jenkins_Agent_Container.
   **/
  agent {
    kubernetes {
      yaml '''
        apiVersion: v1
        kind: Pod
        spec:
          containers:
            - name: node
              image: node:14.19
              command:
                - cat
              tty: true
              volumeMounts:
                - mountPath: /usr/src/app
                  name: shared-data
            - name: docker
              image: docker:latest
              command:
                - cat
              tty: true
              volumeMounts:
                - mountPath: /var/run/docker.sock
                  name: docker-sock
                - mountPath: /usr/src/app
                  name: shared-data
          volumes:
            - name: docker-sock
              hostPath:
                path: /var/run/docker.sock
            - name: shared-data
              emptyDir: {}
      '''
    }
  }
  /**
   ** @description: Publish Global Environment Here.
   ** @variable: DOCKER_HUB_CREDENTIALS.toString() === "<USERNAME>:****"
   ** @variable: DOCKER_HUB_CREDENTIALS_USR === "<USERNAME>"
   ** @variable: DOCKER_HUB_CREDENTIALS_PSW === "****"
   ** @practice:
   ** > Bad : "${DOCKER_HUB_CREDENTIALS_PSW}". Will Trigger String Interpolation from Groovy Scripts.
   ** > Good : '${DOCKER_HUB_CREDENTIALS_PSW}'. Will Not Throw Warning to Jenkins Console Log Output.
   ** @see {@link https://www.jenkins.io/doc/book/using/using-credentials/}
   **/
  environment {
    DOCKER_HUB_CREDENTIALS = credentials('docker-hub')
    SCM_PROVIDER = 'gitlab' // @params: ['github', 'gitlab']. Must be configured as `Gitlab` for this pipeline.
    GIT_USERNAME = 'huynhleminhthinh'
    GIT_PROJECT = 'rest-api'
    GIT_BRANCH = 'v2-develop'
    VERSION_TAG = 'latest'
    VERSION_SUFFIX = '-alpine'
    K8S_DEPLOYMENT = 'rest-api-develop'
    K8S_NAMESPACE = 'rest-api-develop'
    EXECUTE_PIPELINE = true
  }
  /**
   ** ------------------------------------------------------------------------------------------------------------------------------------------------
   ** @description: Retrieve ${WORK_DIR} from Jenkins [Clouds] -> [Pod Templates] -> [Container Templates] -> [Working Directory].
   ** @example > export JENKINS_GIT_WORKDIR="/home/jenkins/agent/workspace/projects/rest-api/pipeline/develop/v2/" ;
   ** @workdir > export JENKINS_AGENT_WORKDIR="/home/jenkins/agent" ;
   ** ------------------------------------------------------------------------------------------------------------------------------------------------
   ** By Default, Jenkins will create sub-directory `/home/jenkins/agent/workspace/<JENKINS_DIRECTORY>/<PROJECT_NAME>`.
   ** For example: $WORK_DIR=`/home/jenkins/agent/workspace/jenkins-root-directory/rest-api`.
   ** This $WORK_DIR will be Container Default Working Directory, and will be shared as Pod Volumes throughout all Containers.
   ** Thus we don't have to manually create [Empty_Dir_Volume:shared-data] as the above Kubernetes Agent Description.
   ** Also, Jenkins Agent already cloned the [Project::`https://gitlab.com/huynhleminhthinh/rest-api.git`] into $WORK_DIR.
   ** @see - GitlabCommitStatus::[pending, running, canceled, success, failed, skipped].
   **/
  stages {
    stage('Pull') {
      when {
        allOf {
          expression { return true } // Trigger Pull Stage.
          expression { return EXECUTE_PIPELINE } // Execute Pull Stage ?
        }
      }
      steps {
        container('node') {
          updateGitlabCommitStatus name: '1. Pull Source', state: 'pending'
          script {
            try {
              VALID_WORK_TREE = sh(script: "git rev-parse --is-inside-work-tree 2> /dev/null", returnStatus: true)
              if (VALID_WORK_TREE != 0) { // Shell command throw error with [Exit_Code != 0]
                sh 'git clone https://${SCM_PROVIDER}.com/${GIT_USERNAME}/${GIT_PROJECT}.git $PWD'
              }
              updateGitlabCommitStatus name: '1. Pull Source', state: 'success'
            } catch (error) {
              echo error.getMessage();
              updateGitlabCommitStatus name: '1. Pull Source', state: 'failed'
              throw error; // Kill All Next Pipeline.
            }
          }
        }
      }
    }
    stage('Checkout') {
      when {
        allOf {
          expression { return true } // Trigger Pull Stage.
          expression { return EXECUTE_PIPELINE } // Execute Checkout Stage ?
        }
      }
      steps {
        /** @see {@link https://gist.github.com/rufoa/2807ad19328f70dc81fec25c317661b8} ~!*/
        container('node') {
          updateGitlabCommitStatus name: '2. Checkout Branch', state: 'pending'
          script {
            try {
              EXEC = sh(script: 'git checkout --track origin/${GIT_BRANCH}', returnStatus: true)
              SKIP = sh(script: "git log -1 | grep '\\[ci-skip\\]'", returnStatus: true)
              if (SKIP == 0) {
                throw new Exception("Skip CI/CD Pipeline.")
              }
              updateGitlabCommitStatus name: '2. Checkout Branch', state: 'success'
            } catch (error) {
              echo error.getMessage();
              currentBuild.result = 'NOT_BUILT'
              updateGitlabCommitStatus name: '2. Checkout Branch', state: 'canceled'
              EXECUTE_PIPELINE = false // Skip All Next Pipeline.
            }
          }
        }
      }
    }
    stage('Install') {
      when {
        allOf {
          expression { return false } // Skip Install Stage.
          expression { return EXECUTE_PIPELINE } // Execute Install Stage ?
        }
      }
      steps {
        container('node') {
          updateGitlabCommitStatus name: '3. Install Packages', state: 'pending'
          script {
            try {
              sh 'npm install' // Install All [Dependencies *& DevDependencies] for Testing.
              updateGitlabCommitStatus name: '3. Install Packages', state: 'success'
            } catch (error) {
              echo error.getMessage();
              updateGitlabCommitStatus name: '3. Install Packages', state: 'failed'
              throw error; // Kill All Next Pipeline.
            }
          }
        }
      }
    }
    stage('Start') {
      when {
        allOf {
          expression { return false } // Skip Start Stage.
          expression { return EXECUTE_PIPELINE } // Execute Start Stage ?
        }
      }
      steps {
        container('node') {
          updateGitlabCommitStatus name: '4. Start App', state: 'pending'
          /* @see {@link https://stackoverflow.com/questions/43452317/jenkins-pipeline-multiline-shell-commands-with-pipe} */
          /* @see {@link https://stackoverflow.com/questions/11904772/how-to-create-a-loop-in-bash-that-is-waiting-for-a-webserver-to-respond} */
          script {
            try {
              sh '''
                attempt_counter=0;
                max_attempts=3;

                npm start &> /dev/null & until $(curl --output /dev/null --silent --head --fail http://localhost:8080); do
                  if [ ${attempt_counter} -eq ${max_attempts} ];then
                    echo "Max attempts reached";
                    exit 1;
                  fi

                  printf "waiting...";
                  attempt_counter=$(($attempt_counter+1))
                  sleep 2;
                done
              '''
              updateGitlabCommitStatus name: '4. Start App', state: 'success'
            } catch (error) {
              echo error.getMessage();
              updateGitlabCommitStatus name: '4. Start App', state: 'failed'
              throw error; // Kill All Next Pipeline.
            }
          }
        }
      }
    }
    stage('Test') {
      when {
        allOf {
          expression { return false } // Skip Test Stage.
          expression { return EXECUTE_PIPELINE } // Execute Test Stage ?
        }
      }
      steps {
        container('node') {
          updateGitlabCommitStatus name: '5. Run Tests', state: 'pending'
          script {
            try {
              sh 'curl --silent localhost:8080' // << Perform Integration Test Here.
              updateGitlabCommitStatus name: '5. Run Tests', state: 'success'
            } catch (error) {
              echo error.getMessage();
              updateGitlabCommitStatus name: '5. Run Tests', state: 'failed'
              throw error; // Kill All Next Pipeline.
            }
          }
        }
      }
    }
    stage('Build') {
      when {
        allOf {
          expression { return true } // Trigger Build Stage.
          expression { return EXECUTE_PIPELINE } // Execute Build Stage ?
        }
      }
      steps {
        container('docker') {
          updateGitlabCommitStatus name: '6. Build Docker Images', state: 'pending'
          script {
            try {
              sh 'docker build ./ --file ./manifests/develop/Dockerfile --tag ${DOCKER_HUB_CREDENTIALS_USR}/${GIT_PROJECT}:${VERSION_TAG}${VERSION_SUFFIX}'
              updateGitlabCommitStatus name: '6. Build Docker Images', state: 'success'
            } catch (error) {
              echo error.getMessage();
              updateGitlabCommitStatus name: '6. Build Docker Images', state: 'failed'
              throw error; // Kill All Next Pipeline.
            }
          }
        }
      }
    }
    stage('Login') {
      when {
        allOf {
          expression { return true } // Trigger Login Stage.
          expression { return EXECUTE_PIPELINE } // Execute Login Stage ?
        }
      }
      steps {
        container('docker') {
          updateGitlabCommitStatus name: '7. Login Docker Registry', state: 'pending'
          script {
            try {
              /*
              withCredentials([string(credentialsId: 'secret-token', variable: 'TOKEN')]) {
                sh 'docker login --username ${USERNAME} --password ${TOKEN}'
              }*/
              withCredentials([
                usernamePassword(credentialsId: 'docker-hub', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')
              ]) {
                sh 'docker login --username ${USERNAME} --password ${PASSWORD}'
              }
              updateGitlabCommitStatus name: '7. Login Docker Registry', state: 'success'
            } catch (error) {
              echo error.getMessage();
              updateGitlabCommitStatus name: '7. Login Docker Registry', state: 'failed'
              throw error; // Kill All Next Pipeline.
            }
          }
        }
      }
    }
    stage('Push') {
      when {
        allOf {
          expression { return true } // Trigger Push Stage.
          expression { return EXECUTE_PIPELINE } // Execute Push Stage ?
        }
      }
      steps {
        container('docker') {
          updateGitlabCommitStatus name: '8. Push Images', state: 'pending'
          script {
            try {
              sh 'docker push ${DOCKER_HUB_CREDENTIALS_USR}/${GIT_PROJECT}:${VERSION_TAG}${VERSION_SUFFIX}'
              updateGitlabCommitStatus name: '8. Push Images', state: 'success'
            } catch (error) {
              echo error.getMessage();
              updateGitlabCommitStatus name: '8. Push Images', state: 'failed'
              throw error; // Kill All Next Pipeline.
            }
          }
        }
      }
    }
    stage('Rollout') {
      when {
        allOf {
          /** @description: Docker Hub Container Image::[${DOCKER_HUB_CREDENTIALS_USR}/${GIT_PROJECT}:${VERSION_TAG}${VERSION_SUFFIX}]
           ** @description: Hence preserve the [${IMAGE_TAG}=`latest-alpine`] forever on the declared Manifests within Development Branch.
           ** @description: Thus we have to manually announce Kubernetes to Rollout Deployments after Pulling [--always] latest Docker Images ~!*/
          expression { return true } // Trigger Rollout Stage. Since [ArgoCD] cannot detect YAML Manifests changes within Gitlab Repository.
          expression { return EXECUTE_PIPELINE } // Execute Rollout Stage ?
        }
      }
      steps {
        container('docker') {
          updateGitlabCommitStatus name: '9. Rollout Deployments', state: 'pending'
          script {
            try {
              /**
               ** @see {@link https://plugins.jenkins.io/kubernetes-cli/}
               ** @see {@link https://technology.amis.nl/platform/kubernetes/jenkins-building-java-and-deploying-to-kubernetes/}
               **/
              withKubeConfig([credentialsId: 'kubernetes-admin-config']) {
                sh 'apk add curl 2> /dev/null'
                sh '''
                  STABLE=$(curl --silent https://storage.googleapis.com/kubernetes-release/release/stable.txt);
                  curl -LO "https://storage.googleapis.com/kubernetes-release/release/$STABLE/bin/linux/amd64/kubectl";
                '''
                sh 'chmod u+x ./kubectl'
                sh './kubectl rollout restart deployment ${K8S_DEPLOYMENT} --namespace ${K8S_NAMESPACE}'
              }
              updateGitlabCommitStatus name: '9. Rollout Deployments', state: 'success'
            } catch (error) {
              echo error.getMessage();
              updateGitlabCommitStatus name: '9. Rollout Deployments', state: 'failed'
              throw error; // Kill All Next Pipeline.
            }
          }
        }
      }
    }
  }
  /**
   ** @description: Always Logout From Docker Account.
   **/
  post {
    always {
      container('docker') {
        sh 'docker logout'
      }
    }
    success {
      echo "BUILD SUCCESS"
    }
    failure {
      echo "BUILD FAILURE"
    }
  }
}
