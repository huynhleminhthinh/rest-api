FROM node:14.19-alpine

# [OPTIONAL] Install Custom Dependency.
RUN apk add --update-cache curl

# [OPTIONAL] Create App Directory.
RUN mkdir -p /home/node/app

# [REQUIRED] Set Current Working Directory ${PWD}.
WORKDIR /home/node/app

# ----------------------------------------------------------------------------------------------------------------------------------------------------
# @description: Docker Image [NodeJS] has already been initiated with [`${user}`:`${group}`] == [`${node}`:`${node}`] within File Systems.
# @see {@link https://docs.docker.com/language/nodejs/build-images/}.
# ----------------------------------------------------------------------------------------------------------------------------------------------------
RUN chown -R node:node /home/node/app

# ----------------------------------------------------------------------------------------------------------------------------------------------------
# @description: Install app dependencies. A wildcard is used to ensure both package.json
# AND package-lock.json are copied where available (npm@5+)
# ----------------------------------------------------------------------------------------------------------------------------------------------------
# [ALTERNATIVE] COPY package*.json /home/node/app/
COPY package*.json ./

# ----------------------------------------------------------------------------------------------------------------------------------------------------
# @description: Install NPM Package with Various Production/Development Choice.
# @see {@link https://stackoverflow.com/questions/9268259/how-do-you-prevent-install-of-devdependencies-npm-modules-for-node-js-package}.
# ----------------------------------------------------------------------------------------------------------------------------------------------------
# [ALTERNATIVE] RUN npm ci --only=production
RUN npm install

# ----------------------------------------------------------------------------------------------------------------------------------------------------
# @description: Push all files within CURRENT_DIRECTORY into DOCKER_IMAGE.
# @see {@link https://testdriven.io/tips/83a02ed7-37e7-435c-a04d-3248341e58a9/}.
# ----------------------------------------------------------------------------------------------------------------------------------------------------
# [ALTERNATIVE] COPY --chown=node:node . /home/node/app/
COPY --chown=node:node ./ ./

# ----------------------------------------------------------------------------------------------------------------------------------------------------
# @description: Remove Manifests Directory within Application File Systems.
# ----------------------------------------------------------------------------------------------------------------------------------------------------
# [EXECUTE ROOT-PRIVILEGE COMMAND] > sudo <command> ;
RUN rm -rf ./manifests

# ----------------------------------------------------------------------------------------------------------------------------------------------------
# @description: Run Application Daemon with Non-Root Privilege.
# @see {@link https://viblo.asia/p/tai-sao-nen-chay-ung-dung-container-voi-non-root-user-jvEla3VNKkw}.
# ----------------------------------------------------------------------------------------------------------------------------------------------------
USER node

# ----------------------------------------------------------------------------------------------------------------------------------------------------
# @description: Expose Container Ports for Outbound Traffic to Request.
# ----------------------------------------------------------------------------------------------------------------------------------------------------
EXPOSE 8080

# [ALTERNATIVE] CMD [ "node", "server.js" ]
CMD [ "npm", "start" ]
