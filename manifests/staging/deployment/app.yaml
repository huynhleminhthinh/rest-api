---
apiVersion: v1
kind: Namespace
metadata:
  name: rest-api-staging

---
apiVersion: v1
kind: ConfigMap
metadata:
  name: api-config
  namespace: rest-api-staging
data:
  config.json: | # This pattern will not create new line at the end of file: `|-`
    {
      "environment" : "staging"
    }

---
apiVersion: v1
kind: Secret
metadata:
  name: api-secret
  namespace: rest-api-staging
type: Opaque
stringData:
  secret.json: | # This pattern will not create new line at the end of file: `|-`
    {
      "api_key" : "123456789"
    }

---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: rest-api-staging
  namespace: rest-api-staging
  labels:
    app: rest-api-staging
  annotations:
spec:
  selector:
    matchLabels:
      app: rest-api-staging
  replicas: 3
  revisionHistoryLimit: 0 # Default to 10 if not specified.
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxSurge: 1
      maxUnavailable: 0
  template:
    metadata:
      labels:
        app: rest-api-staging
    spec:
      containers:
        - name: rest-api-staging
          image: huynhleminhthinh/rest-api:2.0.0-alpine
          imagePullPolicy: Always
          ports:
            - containerPort: 5000
          resources:
            requests:
              memory: "64Mi"
              cpu: "50m"
            limits:
              memory: "256Mi"
              cpu: "500m"
          volumeMounts:
            - name: secret-volume
              mountPath: /secrets/
            - name: config-volume
              mountPath: /configs/
      # ----------------------------------------------------------------------------------------------------------------------------------------------
      # @see {@link https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.24/#volume-v1-core}
      # ----------------------------------------------------------------------------------------------------------------------------------------------
      volumes:
        # --------------------------------------------------------------------------------------------------------------------------------------------
        # @see {@link https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.24/#configmapvolumesource-v1-core}
        # --------------------------------------------------------------------------------------------------------------------------------------------
        - name: secret-volume
          secret:
            secretName: api-secret
        # --------------------------------------------------------------------------------------------------------------------------------------------
        # @see {@link https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.24/#secretvolumesource-v1-core}
        # --------------------------------------------------------------------------------------------------------------------------------------------
        - name: config-volume
          configMap:
            name: api-config

---
apiVersion: v1
kind: Service
metadata:
  name: v2
  # [ALTERNATIVE COMMAND] > kubectl apply --filename *.yaml --namespace=rest-api-staging
  namespace: rest-api-staging
spec:
  selector:
    app: rest-api-staging
  ports:
    # [[PROVIDE SERVICE-NAME WITHIN MULTIPLE SERVICES ENTRY-POINT]] >> @see {@link https://www.youtube.com/watch?v=T4Z7visMM4E&t=766s}
    - name: http
      protocol: TCP
      # [Kubernetes Cluster] - Service Port.
      port: 80
      # [Kubernetes Cluster] - Deployment (Pods) Port.
      targetPort: 8080

---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: rest-api-v2
  namespace: rest-api-staging
  # @description: Ingress Rules: Wildcard & Specific Sub-Domain.
  # @see {@link https://stackoverflow.com/questions/52328483/kubernetes-ingress-rules-how-to-use-wildcard-and-specific-subdomain-together}
  annotations:
    kubernetes.io/tls-acme: "true"
    # @deprecated: Deprecated from Kubernetes v1.22+.
    # @description: Deploying multiple Ingress controllers resulted all controllers simultaneously racing to update Ingress status in confusing ways...
    # @see {@link https://kubernetes.github.io/ingress-nginx/user-guide/multiple-ingress/}
    kubernetes.io/ingress.class: nginx
    # @deprecated: Deprecated {@resources: nginx.ingress.kubernetes.io/add-base-url} from NGINX Ingress Controller.
    # @see {@link https://github.com/kubernetes/ingress-nginx/issues/3770#issuecomment-464449348}
    nginx.ingress.kubernetes.io/rewrite-target: /
    nginx.ingress.kubernetes.io/ssl-redirect: "false"
    nginx.ingress.kubernetes.io/force-ssl-redirect: "false"
spec:
  tls:
    - hosts:
        - v2.staging.rest-api.e8s.io
      secretName: wildcard-rest-api-tls
  rules:
    - host: v2.staging.rest-api.e8s.io
      http:
        paths:
          # @description: In case ${path} differ from ${rewrite-target}. All the following JS, CSS resources would have not been served correctly.
          # @see {@link https://github.com/kubernetes/ingress-nginx/issues/3770#issuecomment-464449348}
          # @deprecated: [- path: /rest-api]
          - path: /
            pathType: Prefix
            backend:
              service:
                name: v2
                port:
                  number: 80

