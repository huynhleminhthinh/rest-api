/* eslint valid-jsdoc: "off", complexity: "off", no-unused-vars: "off", @typescript-eslint/naming-convention: "off" */

/** @ts-nocheck - Turn this on to ignore this file !*/

"use strict";

const bodyParser = require('body-parser');
const express = require('express');

const __APPLICATION__ = require('./package.json');

const PORT = 8080;
const HOST = '0.0.0.0';

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true,
}));

app.get('/*', (req, res) => {
    console.log('> Headers:', req.headers);
    console.log('> Queries:', req.query);
    const contents = [
        `> Hello World - Version [${__APPLICATION__.version}]::[Alpine]`,
        `> Hostname: ${req.headers.host}`,
        `> Url Path: ${req.url}`,
        `> Nonce: ${1}`,
    ].join("\n");
    return res.send(`${contents}\n`);
});

app.post('/*', (req, res) => {
    console.log('> Headers:', req.headers);
    console.log('> Queries:', req.query);
    console.log('> Payload:', req.body);
    const contents = [
        `> Hello World - Version [${__APPLICATION__.version}]::[Alpine]`,
        `> Hostname: ${req.headers.host}`,
        `> Url Path: ${req.url}`,
        `> Nonce: ${1}`,
    ].join("\n");
    return res.send(`${contents}\n`);
});

app.listen(PORT, HOST, () => {
    console.log(`Server is listening on http://${HOST}:${PORT}`);
});
